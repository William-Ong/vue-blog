import showBlog from './components/showBlog';
import addBlog from './components/addBlog';
import singleBlog from './components/singleBlog';

export default [
    {
        path: '/',
        name: 'show-blog',
        component: showBlog
    },
    {
        path: '/add',
        name: 'add-blog',
        component: addBlog
    },
    {
        path: '/blog/:id',
        name: 'single-blog',
        component: singleBlog
    }
]